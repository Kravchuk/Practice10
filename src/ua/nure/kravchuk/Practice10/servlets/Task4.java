package ua.nure.kravchuk.Practice10.servlets;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.kravchuk.Practice10.dao.entity.DAOException;
import ua.nure.kravchuk.Practice10.dao.mysql2.MysqlUserDao;

/**
 * Servlet implementation class Task4
 */
@WebServlet("/Task4")
public class Task4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String BEGIN_PAGE = "task4.jsp";
	private static final String NEXT_PAGE = "task3.jsp";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Task4() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		System.out.print("session id --> " + session.getId());
		System.out.println("; session " + (session.isNew() ? "is new" : "was found"));
		request.setCharacterEncoding("UTF-8");
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		StringBuilder mes = new StringBuilder();
		if(login != null && password != null){
			if(validation(password) && validation(login)){
				doPost(request, response);
			}else{
				mes.append("Your parameters is wrong. Try again");
				request.setAttribute("message", mes);
				response.setContentType("text/html;charset=UTF-8");
				request.getRequestDispatcher(BEGIN_PAGE).forward(request, response);
			}
		}else{
			request.setAttribute("message", mes);
			response.setContentType("text/html;charset=UTF-8");
			request.getRequestDispatcher(BEGIN_PAGE).forward(request, response);
		}
	}

	private boolean validation(String name) {
		boolean res = false;
		Pattern p = Pattern.compile("([a-zA-Zа-яА-Я0-9]+)");
		Matcher m = p.matcher(name);
		if (m.find()) {
			System.out.println(m.group());
			res = m.group().equals(name);
		}
		System.out.println(res);
		return res;
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			HttpSession session = request.getSession(true);
			System.out.print("session id --> " + session.getId());
			System.out.println("; session " + (session.isNew() ? "is new" : "was found"));
			request.setCharacterEncoding("UTF-8");
			String login = request.getParameter("login");
			String password = request.getParameter("password");
			System.out.println(login + password);
			
			StringBuilder mes = new StringBuilder();
			String[] res = null;
			if(login != null && password != null && validation(password) && validation(login)){
				MysqlUserDao udao = new MysqlUserDao();
				try {
					res = udao.findUserByLoginAndPassword(login, password);
				} catch (DAOException e) {
					System.out.println(e.getMessage());
				}
				
				if(res != null){
					session.setAttribute("userId", res[2]);
					session.setAttribute("login", res[1]);
					session.setAttribute("role", res[0]);
					Task3 t3 = new Task3();
					t3.doGet(request, response);
				}else{
					mes.append("Your parameters is wrong. Try again");
					request.setAttribute("message", mes);
					response.setContentType("text/html;charset=UTF-8");
					request.getRequestDispatcher(BEGIN_PAGE).forward(request, response);
				}
			}
	}

}
