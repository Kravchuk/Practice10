package ua.nure.kravchuk.Practice10.servlets;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.kravchuk.Practice10.dao.entity.DAOException;
import ua.nure.kravchuk.Practice10.dao.mysql2.MysqlUserDao;

/**
 * Servlet implementation class Task6
 */
@WebServlet("/Task6")
public class Task6 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Task6() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request.getRequestDispatcher("task6.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		String id = (String) session.getAttribute("userId");
		request.setCharacterEncoding("UTF-8");
		String newName = request.getParameter("name");
		MysqlUserDao udao = new MysqlUserDao();
		StringBuilder message2 = new StringBuilder();
		System.out.println(newName+id);
		
		if(newName != null && id != null){
			if(validation(newName)){
				try {
					boolean res = udao.updateUserName(newName, Integer.parseInt(id));
					if(res = true){
						message2.append("Updated successfully");
					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (DAOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				message2.append("Wrong name");
			}
		}else{
			message2.append("Wrong parameters");
		}
		request.setAttribute("message2", message2);
		request.getRequestDispatcher("task6.jsp").forward(request, response);
	}

	private boolean validation(String name) {
		boolean res = false;
		Pattern p = Pattern.compile("([a-zA-Zа-яА-Я0-9]+)");
		Matcher m = p.matcher(name);
		if (m.find()) {
			System.out.println(m.group());
			res = m.group().equals(name);
		}
		System.out.println(res);
		return res;
	}
}
