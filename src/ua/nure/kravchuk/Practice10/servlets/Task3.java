package ua.nure.kravchuk.Practice10.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Task3
 */
@WebServlet("/Task3")
public class Task3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String BEGIN_PAGE = "task3.jsp";
	private static final String ANSWER_PAGE = "/task3.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Task3() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("doGet");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);
		StringBuilder message = new StringBuilder();
		List<String> list = null;
		
		System.out.print("session id --> " + session.getId());
		System.out.println("; session " + (session.isNew() ? "is new" : "was found"));
		
		if (session.getAttribute("list") != null) {
			list = (List<String>) session.getAttribute("list");
		} else {
			list = new ArrayList<>();
		}
		
		session.setAttribute("list", list);
		String login = (String) session.getAttribute("login");
		String role =  (String) session.getAttribute("role");
		
		if(role!=null && login !=null){
			System.out.println("not null");
			message.append("You are logged as ").append(role).append(" ").append(login);
			request.setAttribute("message", message);
		}
		
		if (request.getParameter("name") == null) {
			if (request.getParameter("remove") == null) {
				request.getRequestDispatcher(BEGIN_PAGE).forward(request, response);
			} else {
				List<String> l = new ArrayList<>();
				session.setAttribute("list", l);
				request.getRequestDispatcher(BEGIN_PAGE).forward(request, response);
			}
		} else {
			doPost(request, response);
		}
	}

	private boolean nameValidation(String name) {
		String s = name;
		boolean res = false;
		Pattern p = Pattern.compile("([a-zA-Zа-яА-Я0-9]+)");
		Matcher m = p.matcher(s);
		if (m.find()) {
			res = m.group().equals(s);
		}
		return res;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("doPost");
		List<String> list = null;
		StringBuilder message = new StringBuilder();
		HttpSession session = request.getSession(true);
		request.setCharacterEncoding("UTF-8");
		System.out.print("session id --> " + session.getId());
		System.out.println("; session " + (session.isNew() ? "is new" : "was found"));
		String name = request.getParameter("name");
	
		String login = (String) session.getAttribute("login");
		String role =  (String) session.getAttribute("role");
		
		if(role!=null && login !=null){
			System.out.println("not null");
			message.append("You are logged as ").append(role).append(" ").append(login);
			request.setAttribute("message", message);
		}
		
		if (request.getParameter("remove") != null) {
			List<String> l = new ArrayList<>();
			session.setAttribute("list", l);
			request.setAttribute("message", message);
			request.getRequestDispatcher(BEGIN_PAGE).forward(request, response);
		} else {
			list = (List<String>) session.getAttribute("list");
			if (list == null) {
				list = new ArrayList<>();
			}

			if (nameValidation(name)) {
				list.add(name);
				session.setAttribute("list", list);
				request.setAttribute("list", list);
			}
			
			request.setAttribute("message", message);
			getServletContext().getRequestDispatcher(ANSWER_PAGE).forward(request, response);

		}
	}

}
