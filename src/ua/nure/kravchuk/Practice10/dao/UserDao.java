package ua.nure.kravchuk.Practice10.dao;

import ua.nure.kravchuk.Practice10.dao.entity.DAOException;

public interface UserDao {
	public String[] findUserByLoginAndPassword(String login, String password) throws DAOException;
	public String getUsersRole(int id)throws DAOException;
	public boolean updateUserName(String newName, int id)throws DAOException;
}
