package ua.nure.kravchuk.Practice10.dao.mysql2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.kravchuk.Practice10.dao.UserDao;
import ua.nure.kravchuk.Practice10.dao.entity.DAOException;
import ua.nure.kravchuk.Practice10.dao.transaction.ConnectionManager;
import ua.nure.kravchuk.Practice10.entity.User;

public class MysqlUserDao implements UserDao {
	private static final String SQL_FIND_USER = "SELECT * FROM `practice10`.`users` WHERE login=? AND password=?";
	private static final String SQL_SELECT_USER_ROLE = "SELECT `roles`.`name` FROM `practice10`.`roles` WHERE id=?";
	private static final String SQL_UPDATE_USERS_NAME = "UPDATE  `practice10`.`users` SET name=? WHERE user_id=?";
	private Connection con;
	private ConnectionManager instance = new ConnectionManager();
	

	@Override
	public String[] findUserByLoginAndPassword(String login, String password) throws DAOException {
		String[] loggedIn = new String[3];
		try {
			con = instance.getConnection();
			User user = findUserByLP(con, login, password);
			if (user != null) {
				loggedIn[1] = user.getLogin();
				loggedIn[2] = "" + user.getUserId();
			}else{
				return null;
			}

			String role = getUserRole(con, user.getRole());
			if (role != null && role.length() > 1) {
				loggedIn[0] = role;
			}else{
				return null;
			}

			return loggedIn;
		} catch (SQLException ex) {
			// (1) LOG.error("Cannot find all users, ex)

			// (2)
			throw new DAOException("Cannot obtain a user", ex);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				throw new DAOException("Cannot close connection", e);
			}
		}

	}

	private User findUserByLP(Connection con, String login, String password) throws SQLException {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = con.prepareStatement(SQL_FIND_USER);
			pstmt.setString(1, login);
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}

			return user;
		} finally {
			rs.close();
			pstmt.close();
		}
	}

	public User extractUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setUserId(rs.getInt("user_id"));
		user.setLogin(rs.getString("login"));
		user.setPassword(rs.getString("password"));
		user.setRole(rs.getInt("role_id"));
		return user;
	}

	@Override
	public String getUsersRole(int id) throws DAOException {
		try {
			con = instance.getConnection();
			String res = getUserRole(con, id);
			return res;
		} catch (SQLException ex) {
			// (1) LOG.error("Cannot find all users, ex)

			// (2)
			throw new DAOException("Cannot obtain a role", ex);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				throw new DAOException("Cannot close connection", e);
			}
		}
	}

	private String getUserRole(Connection con2, int id) throws SQLException {
		String role = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = con.prepareStatement(SQL_SELECT_USER_ROLE);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				role = rs.getString("name");
			}
			return role;
		} finally {
			rs.close();
			pstmt.close();
		}
	}

	@Override
	public boolean updateUserName(String newName, int id) throws DAOException {
			try {
				con = instance.getConnection();
				con.setAutoCommit(false);
				con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

				int res = updateUserById(con, newName, id);
				if (res == 0) {
					System.out.println("update fails");
					return false;
				} else {
					System.out.printf("Updated %d cells.\n", res);
					con.commit();
					return true;
				}
			} catch (SQLException ex) {
				// (1) LOG.error("Cannot update user, ex)
				//
				if (con != null) {
					try {
						con.rollback();
					} catch (SQLException e) {
						// log ==> cannot rollback transaction
					}
				}

				// (2)
				throw new DAOException("Cannot update user", ex);
			} finally {
				try {
					con.close();
				} catch (SQLException ex) {
					// (1) LOG.error("Cannot update user", ex)
					System.out.println("Error in closing connection or statement");
				}
			}
		}

		private int updateUserById(Connection con, String newName, int id) throws SQLException {
			PreparedStatement pstmt = null;
			try {
				pstmt = con.prepareStatement(SQL_UPDATE_USERS_NAME);
				int k = 0;
				pstmt.setString(++k, newName);
				pstmt.setInt(++k, id);
				int res = pstmt.executeUpdate();

				return res;
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				return 0;
			} finally {
				pstmt.close();
			}

		}

}
