package ua.nure.kravchuk.Practice10.dao.transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import ua.nure.kravchuk.Practice10.dao.entity.DAOException;

public class ConnectionManager {
	private DataSource ds;

	public ConnectionManager() {
		try {
			initializeDataSource();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		prop.setProperty("charSet", "UTF-8");
	}

	public Connection getConnection() throws DAOException {
		Connection con = null;
		try {
			con = ds.getConnection();
		} catch (SQLException e) {
			throw new DAOException("Couldn't get connection", e);
		}
		return con;
	}

	private void initializeDataSource() throws DAOException {

		Context initCtx;
		try {
			initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			ds = (DataSource) envCtx.lookup("jdbc/practice10");
		} catch (NamingException e) {
			throw new DAOException("Couldn't initialize context", e);
		}
	}

}
