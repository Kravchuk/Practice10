package ua.nure.kravchuk.Practice10.dao.entity;

public class DAOException extends Exception {
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

}
