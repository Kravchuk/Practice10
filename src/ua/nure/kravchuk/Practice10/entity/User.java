package ua.nure.kravchuk.Practice10.entity;

public class User {
	private int userId;

	private String login;

	private String password;
	
	private String name;

	private int role;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "User [user_id = " + userId + ", login = " + login + ", password" + password + ", role = " + role + ", name= "+ name + "]";
	}

	public String toStringSimple() {
		return "User [user_id = " + userId + ", login = " + login + ", password" + password + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
