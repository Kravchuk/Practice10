<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Task3</title>
<style>
h3 {
	width: 350px;
	height: 200px;
	position: fixed;
	top: 0;
	right: 0;
}
</style>
</head>
<body>
	<c:if test="${message != null}">
		<a href='./task1'>Task1</a>
		<a href='./task2'>Task2</a>
		<a href='./task3'>Task3</a>
		<a href='./task6'>Task4</a>
		<h3>
			<c:out value="${message}"></c:out>
		</h3>
	</c:if>
	<p />
	<fieldset>
		<legend>Input:</legend>
		<form method="post" action="Task3">
			<input name="name" placeholder="Your name" required
				pattern="([a-zA-Zа-яА-Я0-9]+)"> <input type="submit"
				value="Go">
		</form>
	</fieldset>
	<p />
	<fieldset>
		<legend>Remove:</legend>
		<a href='./task3?remove=true'>Remove</a>
	</fieldset>
	<p />
	<c:if test="${list !=null && list.size() > 0}">
		<fieldset>
			<legend>Names:</legend>

			<c:set var="list" value="${list}" />

			<p />
			<c:forEach var="name" items="${list}">
				<p>${name}</p>
			</c:forEach>
		</fieldset>
	</c:if>

</body>
</html>