<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Task6</title>
<style>
h3 {
	width: 350px;
	height: 200px;
	position: fixed;
}
</style>
</head>
<body>
	<c:if test="${message != null}">
		<h3>
			<c:out value="${message}"></c:out>
		</h3>
	</c:if>

	<fieldset>
		<legend>Change name:</legend>
		<form method="post" action="Task6">
			<input name="name" placeholder="New name" required
				pattern="([a-zA-Zа-яА-Я0-9]+)"> <input type="submit"
				value="Change">
		</form>
	</fieldset>
	<p />
	<c:if test="${message2 != null}">
		<fieldset>
			<p>
				<c:out value="${message2}"></c:out>
			</p>
		</fieldset>
		<p />
	</c:if>
	<fieldset>
		<legend>Back:</legend>
		<a href='./task3'>Task3</a>
	</fieldset>
</body>
</html>