<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Task4</title>
<style>
h3 {
	width: 350px;
	height: 200px;
	position: fixed;
	top: 0;
	right: 0;
}
</style>
</head>
<body>
	<c:if test="${message != null}">
		<h3>
			<c:out value="${message}"></c:out>
		</h3>
	</c:if>

	<fieldset>
		<legend>Log in:</legend>
		<form method="post" action="Task4">
			<input name="login" placeholder="Your login" required
				pattern="([a-zA-Zа-яА-Я0-9]+)"> <input name="password"
				placeholder="Your password" required pattern="([a-zA-Zа-яА-Я0-9]+)">
			<input type="submit" value="Login">
		</form>
	</fieldset>
	<p />

</body>
</html>