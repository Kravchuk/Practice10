<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Multiplication table</title>
</head>
<body>
	<table border="1">
		<caption>Multiplication table</caption>
		<%
			for (int i = 0; i < 10; i++) {
		%>
		<tr>
			<%
				for (int j = 0; j < 10; j++) {
			%>
			<%
				if (i != 0 && j != 0) {
			%>
			<td><%=i * j%></td>
			<%
				} else if (i == 0 && j != 0) {
			%>
			<td><%=j%></td>
			<%
				} else if (i != 0 && j == 0) {
			%>

			<td><%=i%></td>
			<%
				}else{
			%>
			<td></td>
			<%
				}
			%>
			<%
				}
			%>
		</tr>
		<%
			}
		%>
	</table>
</body>
</html>