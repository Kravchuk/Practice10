<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Multiplication table JSTL</title>
</head>
<body>
	<table border="1">
		<caption>Multiplication table JSTL</caption>

		<c:forEach var="i" begin="0" end="9">
			<tr>
				<c:forEach var="j" begin="0" end="9">
					<c:choose>
						<c:when test="${i != 0 && j != 0 }">
							<td>${j*i}</td>
						</c:when>
						<c:when test="${i == 0 && j != 0 }">
							<td>${j}</td>
						</c:when>
						<c:when test="${i != 0 && j == 0 }">
							<td>${i}</td>
						</c:when>
						<c:otherwise>
							<td></td>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</tr>
		</c:forEach>


	</table>
</body>
</html>