<%@ page isErrorPage="true" import="java.io.*" language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error</title>
<link rel="stylesheet" href="./css/styles.css" type="text/css" />
</head>
<body>
	<table class="title">
		<tr>
			<th>ERROR</th>
		</tr>
	</table>
	<p />
	<fieldset>
		<legend>Description</legend>
		</p>
		<%if(exception.getMessage() != null){%>
		<%=exception.getMessage()%>
		<%} %>
	</fieldset>
	<p />
	<fieldset>
		<legend>Back</legend>
		<p />
		<a href='index.html'>begin</a>
	</fieldset>
</body>
</html>